﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using University.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace University.DAL
{
    public class UniversityContext : DbContext
    {
        public UniversityContext() : base("UniversityContext")
        {

        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Curator> Curators { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Teacher> Teachers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Subject>()
             .HasMany(c => c.Teacher).WithMany(i => i.Subject)
            .Map(t => t.MapLeftKey("SubjectID")
             .MapRightKey("ID")
             .ToTable("SubjectTeacher"));
        }
    }
}