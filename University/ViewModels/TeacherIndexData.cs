﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using University.Models;

namespace University.ViewModels
{
    public class TeacherIndexData
    {
        public IEnumerable<Teacher> Teachers { get; set; }
        public IEnumerable<Subject> Subjects { get; set; }
    }
}