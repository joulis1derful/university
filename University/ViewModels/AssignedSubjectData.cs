﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace University.ViewModels
{
    public class AssignedSubjectData
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public bool Assigned { get; set; }
    }
}