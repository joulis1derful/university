namespace University.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Curator",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LastName = c.String(nullable: false, maxLength: 50),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        Tel = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Group",
                c => new
                    {
                        CuratorID = c.Int(nullable: false),
                        GroupName = c.String(),
                        StudentsAmount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CuratorID)
                .ForeignKey("dbo.Curator", t => t.CuratorID)
                .Index(t => t.CuratorID);
            
            CreateTable(
                "dbo.Student",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LastName = c.String(nullable: false, maxLength: 50),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        isGettingPaid = c.Boolean(nullable: false),
                        Group_CuratorID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Group", t => t.Group_CuratorID)
                .Index(t => t.Group_CuratorID);
            
            CreateTable(
                "dbo.Subject",
                c => new
                    {
                        SubjectID = c.Int(nullable: false, identity: true),
                        SubjectName = c.String(),
                        ClassesAmount = c.Int(nullable: false),
                        Student_ID = c.Int(),
                    })
                .PrimaryKey(t => t.SubjectID)
                .ForeignKey("dbo.Student", t => t.Student_ID)
                .Index(t => t.Student_ID);
            
            CreateTable(
                "dbo.Teacher",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LastName = c.String(nullable: false, maxLength: 50),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        Degree = c.String(),
                        isTrainee = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SubjectTeacher",
                c => new
                    {
                        SubjectID = c.Int(nullable: false),
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SubjectID, t.ID })
                .ForeignKey("dbo.Subject", t => t.SubjectID, cascadeDelete: true)
                .ForeignKey("dbo.Teacher", t => t.ID, cascadeDelete: true)
                .Index(t => t.SubjectID)
                .Index(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubjectTeacher", "ID", "dbo.Teacher");
            DropForeignKey("dbo.SubjectTeacher", "SubjectID", "dbo.Subject");
            DropForeignKey("dbo.Subject", "Student_ID", "dbo.Student");
            DropForeignKey("dbo.Student", "Group_CuratorID", "dbo.Group");
            DropForeignKey("dbo.Group", "CuratorID", "dbo.Curator");
            DropIndex("dbo.SubjectTeacher", new[] { "ID" });
            DropIndex("dbo.SubjectTeacher", new[] { "SubjectID" });
            DropIndex("dbo.Subject", new[] { "Student_ID" });
            DropIndex("dbo.Student", new[] { "Group_CuratorID" });
            DropIndex("dbo.Group", new[] { "CuratorID" });
            DropTable("dbo.SubjectTeacher");
            DropTable("dbo.Teacher");
            DropTable("dbo.Subject");
            DropTable("dbo.Student");
            DropTable("dbo.Group");
            DropTable("dbo.Curator");
        }
    }
}
