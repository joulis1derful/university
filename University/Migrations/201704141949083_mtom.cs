namespace University.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mtom : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Group", "GroupName", c => c.String(nullable: false));
            AlterColumn("dbo.Subject", "SubjectName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Subject", "SubjectName", c => c.String());
            AlterColumn("dbo.Group", "GroupName", c => c.String());
        }
    }
}
