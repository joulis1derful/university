﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace University.Models
{
    public class Group
    {
        [Key]
        [ForeignKey("Curator")]
        public int CuratorID { get; set; }
        [Required]
        public string GroupName { get; set; }
        [Required]
        public int StudentsAmount { get; set; }

        public virtual Curator Curator { get; set; }
        public virtual ICollection<Student> Student { get; set; }
    }
}