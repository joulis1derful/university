﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace University.Models
{
    public class Subject
    {
        public int SubjectID { get; set; }
        [Required]
        public string SubjectName { get; set; }
        [Required]
        public int ClassesAmount { get; set; }

        public virtual Student Student { get; set; }
        public virtual ICollection<Teacher> Teacher { get; set; }

        public Subject()
        {
            Teacher = new List<Teacher>();
        } 
          
    }
}