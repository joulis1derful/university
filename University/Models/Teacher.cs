﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace University.Models
{
    public class Teacher : Person
    {
        public int ID { get; set; }
       
        public string Degree { get; set; }
        public bool isTrainee { get; set; }

        public virtual ICollection<Subject> Subject { get; set; }

        public Teacher()
        {
            Subject = new List<Subject>();
        }
    }
}