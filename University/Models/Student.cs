﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace University.Models
{
    public class Student : Person
    {
        public int ID { get; set; }
        
        public string FirstName { get; set; }
        public bool isGettingPaid { get; set; }

        public virtual ICollection<Subject> Subject { get; set; }
        public virtual Group Group { get; set; }
    }
}