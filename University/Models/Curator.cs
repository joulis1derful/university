﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace University.Models
{
    public class Curator : Person
    {
        public int ID { get; set; }
        
        public string Tel { get; set; }

        public virtual Group Group { get; set; }
     }
}