﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using University.DAL;
using University.Models;

namespace University.Controllers
{
    public class CuratorsController : Controller
    {
        private UniversityContext db = new UniversityContext();

        // GET: Curators
        public ActionResult Index()
        {
            var curators = db.Curators.Include(c => c.Group);
            return View(curators.ToList());
        }

        // GET: Curators/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Curator curator = db.Curators.Find(id);
            if (curator == null)
            {
                return HttpNotFound();
            }
            return View(curator);
        }

        // GET: Curators/Create
        public ActionResult Create()
        {
            ViewBag.ID = new SelectList(db.Groups, "CuratorID", "GroupName");
            return View();
        }

        // POST: Curators/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,LastName,FirstName,Tel")] Curator curator)
        {
            if (ModelState.IsValid)
            {
                db.Curators.Add(curator);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID = new SelectList(db.Groups, "CuratorID", "GroupName", curator.ID);
            return View(curator);
        }

        // GET: Curators/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Curator curator = db.Curators.Find(id);
            if (curator == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID = new SelectList(db.Groups, "CuratorID", "GroupName", curator.ID);
            return View(curator);
        }

        // POST: Curators/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,LastName,FirstName,Tel")] Curator curator)
        {
            if (ModelState.IsValid)
            {
                db.Entry(curator).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID = new SelectList(db.Groups, "CuratorID", "GroupName", curator.ID);
            return View(curator);
        }

        // GET: Curators/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Curator curator = db.Curators.Find(id);
            if (curator == null)
            {
                return HttpNotFound();
            }
            return View(curator);
        }

        // POST: Curators/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Curator curator = db.Curators.Find(id);
            db.Curators.Remove(curator);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
