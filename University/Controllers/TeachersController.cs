﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using University.DAL;
using University.Models;
using University.ViewModels;

namespace University.Controllers
{
    public class TeachersController : Controller
    {
        private UniversityContext db = new UniversityContext();

        // GET: Teachers
        public ActionResult Index(int? id, int? subjectID)
        {
            var viewModel = new TeacherIndexData();
            viewModel.Teachers = db.Teachers
                .Include(i => i.Subject)
                .OrderBy(i => i.LastName);

            if (id != null)
            {
                ViewBag.SubjectID = id.Value;
                viewModel.Subjects = viewModel.Teachers.Where(
                    i => i.ID == id.Value).Single().Subject;
            }

            if (subjectID != null)
            {
                ViewBag.SubjectID = subjectID.Value;
            
            }

            return View(viewModel);
        }

        // GET: Teachers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teacher teacher = db.Teachers.Find(id);
            if (teacher == null)
            {
                return HttpNotFound();
            }
            return View(teacher);
        }

        // GET: Teachers/Create
        public ActionResult Create()
        {
            var teacher = new Teacher();
            teacher.Subject = new List<Subject>();
            PopulateAssignedSubjectData(teacher);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LastName,FirstName,Degree,isTrainee,SubjectName")]Teacher teacher, string[] selectedSubjects)
        {
            if (selectedSubjects != null)
            {
                teacher.Subject = new List<Subject>();
                foreach (var subject in selectedSubjects)
                {
                    var subjectToAdd = db.Subjects.Find(int.Parse(subject));
                    teacher.Subject.Add(subjectToAdd);
                }
            }
            if (ModelState.IsValid)
            {
                db.Teachers.Add(teacher);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PopulateAssignedSubjectData(teacher);
            return View(teacher);
        }

        // GET: Teachers/Edit/5
        public ActionResult Edit(int id = 0)
        {
            Teacher teacher = db.Teachers.Find(id);
            if (teacher == null)
            {
                return HttpNotFound();
            }
            ViewBag.Subjects = db.Subjects.ToList();
            return View(teacher);
        }

        private void PopulateAssignedSubjectData(Teacher teacher)
        {
            var allSubjects = db.Subjects;
            var teacherSubjects = new HashSet<int>(teacher.Subject.Select(s => s.SubjectID));
            var viewModel = new List<AssignedSubjectData>();
            foreach (var subject in allSubjects)
            {
                viewModel.Add(new AssignedSubjectData
                {
                    SubjectID = subject.SubjectID,
                    SubjectName = subject.SubjectName,
                    Assigned = teacherSubjects.Contains(subject.SubjectID)
                });
            }
            ViewBag.Subjects = viewModel;
        }

        // POST: Teachers/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(Teacher teacher, int[] selectedSubjects)
        {
            Teacher newTeacher = db.Teachers.Find(teacher.ID);
            newTeacher.LastName = teacher.LastName;
            newTeacher.FirstName = teacher.FirstName;

            newTeacher.Subject.Clear();
            if (selectedSubjects != null)
            {
                //получаем выбранные курсы
                foreach (var c in db.Subjects.Where(co => selectedSubjects.Contains(co.SubjectID)))
                {
                    newTeacher.Subject.Add(c);
                }
            }

            db.Entry(newTeacher).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        private void UpdateTeacherSubjects(string[] selectedSubjects, Teacher teacherToUpdate)
        {
            if (selectedSubjects == null)
            {
                teacherToUpdate.Subject = new List<Subject>();
                return;
            }

            var selectedSubjectsHS = new HashSet<string>(selectedSubjects);
            var teacherSubjects = new HashSet<int>
                (teacherToUpdate.Subject.Select(s => s.SubjectID));
            foreach (var subject in db.Subjects)
            {
                if (selectedSubjectsHS.Contains(subject.SubjectID.ToString()))
                {
                    if (!teacherSubjects.Contains(subject.SubjectID))
                    {
                        teacherToUpdate.Subject.Add(subject);
                    }
                }
                else
                {
                    if (teacherSubjects.Contains(subject.SubjectID))
                    {
                        teacherToUpdate.Subject.Remove(subject);
                    }
                }
            }
        }

        // GET: Teachers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teacher teacher = db.Teachers.Find(id);
            if (teacher == null)
            {
                return HttpNotFound();
            }
            return View(teacher);
        }

        // POST: Teachers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Teacher teacher = db.Teachers.Find(id);
            db.Teachers.Remove(teacher);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
